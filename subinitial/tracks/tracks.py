# -*- coding: UTF-8 -*-
# LAN Tracks interface
# © 2012-2020 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

from .landevice import *


_channel_tostr = ('A0', 'A1', 'A2', 'A3', 'B0', 'B1', 'B2', 'B3', 'C0', 'C1', 'C2', 'C3', 'D0', 'D1', 'D2', 'D3',
                  'E0', 'E1', 'E2', 'E3', 'F0', 'F1', 'F2', 'F3', 'G0', 'G1', 'G2', 'G3', 'H0', 'H1', 'H2', 'H3')

_channelweight = {'A': 0, 'B': 4, 'C': 8, 'D': 12, 'E': 16, 'F': 20, 'G': 24, 'H': 28}


def _idx_to_mask(idx):
    if type(idx) == str:  # Decode string types
        if len(idx) != 2:
            return 0
        id_ = _channelweight.get(idx[0].upper(), None)
        if id_ is None:
            return 0
        id_ += ord(idx[1]) - ord('0')
        idx = id_ if id_ >= 0 else 0
    if idx < 32:
        return 1 << idx
    return 0


class Advanced(Scope):
    def channelgroups(self, *channelgroups: int):
        """Defines a set of ChannelGroups on this Tracks device.

        Any previous ChannelGroup configuration on Tracks will be overwritten by this configuration.
        All channels associated with a ChannelGroup will be disengaged immediately.
        ChannelGroups are cleared between power-cycles/reboots.

        :param channelgroups: Each argument is a 32-bit integer whose bits represent a collection of channels on the
            Tracks device. One ChannelGroup will be registered on Tracks for each argument supplied. Only one
            channel within a ChannelGroup may be engaged at a time. An attempt to engage more than one channel within
            a ChannelGroup will result in a Python Exception 'ChannelGroup violation'.
            For convenience, use the _select_ method to ensure only one channel within a ChannelGroup is engaged at a
            time.
        """
        self._request(":advanced:channelgroups", *channelgroups)

    def get_channelgroups(self) -> List[int]:
        """Get all active ChannelGroups configured for this TracksDevice.

        :return: channelmasks for each active ChannelGroup configured on this Tracks device. An empty list indicates
        no active ChannelGroups have been defined.
        """
        resp = self._request(":advanced:channelgroups?")
        return resp

    def select(self, channel: int):
        """Engage only one channel at a time with respect to the channel's ChannelGroup membership.

        :param channel: channel to be engaged.
            If this channel is a ChannelGroup member then its ChannelGroup channels are disengaged completely before
            this channel is engaged. If the channel provided is not a ChannelGroup member then all other channels
            not part of a ChannelGroup are disengaged completely before this channel is engaged.
        """
        self._request(":advanced:select", channel)


class Tracks(LanDevice):
    ASSEMBLY = "SA13980"
    FIRMWARE_MIN = (2, 0, 0)
    ALL_CHANNELS_MASK = 0xFFFFFFFF

    def __init__(self, host: str, autoconnect=True):
        """Construct a Subinitial Tracks instance"""
        LanDevice.__init__(self, host, autoconnect)

        # Scopes
        self.advanced = Advanced(self)

    def cmd(self, *channels: int, mask=0):
        """Command specified channels on (and disengage omitted channels).

        :param channels: arbitrary list of indices. e.g. 0, 1, 12, 13
        :param mask: optional bitmask of indices. e.g. 0b11
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        """
        bits = digest_indices(channels, mask, idx2mask=_idx_to_mask)
        self._request(":cmd", bits)

    def set(self, *channels: int, mask=0):
        """Engage relay channels.

        :param channels: arbitrary list of indices. e.g. 0, 1, 12, 13
        :param mask: optional bitmask of indices. e.g. 0b11
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int
        """
        bits = digest_indices(channels, mask, idx2mask=_idx_to_mask)
        self._request(":set", bits)

    def clear(self, *channels: int, mask=0):
        """Disengage relay channels.

        :param channels: arbitrary list of indices. e.g. 0, 1, 12, 13
        :param mask: optional bitmask of indices. e.g. 0b11
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        """
        bits = digest_indices(channels, mask, idx2mask=_idx_to_mask)
        self._request(":clear", bits)

    def toggle(self, *channels: int, mask=0):
        """toggle relay channels

        :param channels: arbitrary list of indices. e.g. 0, 1, 12, 13
        :param mask: optional bitmask of indices. e.g. 0b11
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        """
        bits = digest_indices(channels, mask, idx2mask=_idx_to_mask)
        self._request(":toggle", bits)

    CMDTYPE_MASK = 0
    CMDTYPE_NAMES = 1
    CMDTYPE_INDICES = 2

    def get_cmd(self, cmdtype=CMDTYPE_MASK) -> Bitmask or list[int]:
        """Get commanded (engaged) channels.

        :param cmdtype: selects return type.

            * 0: bitmask
            * 1: list of names
            * 2: list of indices

        :return: command channels as bitmask, names, or indices
        """
        cmdmask = int(self._request(":cmd?"))
        if cmdtype == self.CMDTYPE_MASK:
            return Bitmask(cmdmask)
        cmdbits = mask_to_bits(cmdmask)
        if cmdtype == self.CMDTYPE_INDICES:
            return cmdbits
        return list(_channel_tostr[i] for i in cmdbits)
