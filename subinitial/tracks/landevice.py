# -*- coding: UTF-8 -*-
# Subinitial Library Components
# © 2012-2020 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with Subinitial Hardware products.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import sys
import socket
import time
import threading
import json
import struct
from typing import List, Tuple, Dict, Set, NamedTuple, Callable, Union

LIB_VERSION = (2, 0, 0)

BUFFER_SIZE = 4096
NO_CHANGE = None
DEFAULT = None


class SubinitialException(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return repr(self.msg)


class SubinitialTimeoutError(SubinitialException):
    pass


class SubinitialInputError(SubinitialException):
    pass


class SubinitialResponseError(SubinitialException):
    def __init__(self, msg, tag=None, error_code=0):
        SubinitialException.__init__(self, msg)
        self.tag = tag
        self.error_code = error_code

    def __str__(self):
        return "{}, tag: {}".format(repr(self.msg), repr(self.tag))


class SubinitialResponseErrors(SubinitialException):
    def __init__(self, errors):
        self.errors = errors

    def __str__(self):
        if self.errors is None:
            return "No Errors Found"
        return "\n".join(map(lambda x: repr(x), self.errors))


def to_bool(astr) -> bool:
    if astr is None:
        return False
    if type(astr) == bytes:
        astr = astr.decode("utf-8")
    if type(astr) != str:
        return bool(astr)
    astr = astr.lower().strip()
    if len(astr) == 0 or astr in ("0", "false", "n", "no", "none", "null"):
        return False
    return True


class NullLogger:
    def warning(self, astr: str):
        pass


class WarnLogger:
    def warning(self, astr: str):
        sys.stderr.write(astr)
        sys.stderr.write("\n")


def _protocol_change(protocol_id, flags=0):
    return struct.pack(">BBH", 0x10, flags, protocol_id)


def _json2bytes(obj):
    sobj = json.dumps(obj).encode("ascii")
    return struct.pack(">H", len(sobj)) + sobj


def _bytes2json(b):
    msglen, = struct.unpack_from(">H", b, 0)
    if len(b) < msglen + 2:
        raise Exception("invalid jsonmsg header")
    astr = b[2:2+msglen].decode("ascii")
    return json.loads(astr)


class Connection:
    FAILUREMODE_RECONNECT = 0
    FAILUREMODE_SESSIONID_CHANGED = 1
    FAILUREMODE_NO_RETRIES_REMAIN = 2

    @property
    def ipaddr(self):
        if self._ipaddr is None:
            self._ipaddr = socket.gethostbyname(self.host)
        return self._ipaddr

    @property
    def host(self):
        return self._host

    @host.setter
    def host(self, val):
        self._host = val
        self._ipaddr = None

    def __init__(self, device: "LanDevice", host: str):
        self.device = device
        self.devicename = self.device.__class__.__name__
        self.socket = None
        self.is_connected = False
        self.raise_response_exceptions = True
        self._ipaddr = None
        self._host = None
        self.host = host
        self.port = 9221
        self.protocol = 1
        self.retries = 3
        """ retries: number of reconnection attempts (per each request) to perform after a network failure
            with the device. Set this to None for an infinite number of retries """
        self.timeout = 5.0
        """ timeout: time in seconds after which a socket operation is considered timed out thereby prompting a
            reconnection attempt or raising a SubinitialNetworkException if no retries remain """
        self.on_networkfailure: Callable[[Connection, int], int or None] or None = None
        """ on_failure: An optional event handler which is called during network failures to allow user
            intervention and custom fault tolerant applications

                * Param 1 (NetworkConnection): A reference to the failing network connection is supplied.
                  Use this to modify the NetworkConnection host, port, or timeout fields before continuing with
                  reconnection attempts.
                * Param 2 (int) FailureMode: A flag that notifies what failure mode is to be handled.

                    * Mode 0: A network timeout has occured and a reconnection attempt is about to be made.

                        * return None to continue recovering from the network failure
                        * return 0 to cancel the attempt and raise a SubinitialNetworkException
                        * return 1+ to specify the number of additional attempts remaining, thereby prolonging the
                          reconnection attempt procedure.

                    * Mode 1: A reconnection attempt has succeeded, but the device's session ID has changed
                      changed indicating possible state resets.

                        * return None to continue recovering from the network failure
                        * return 0 to cancel the attempt and raise a SubinitialNetworkException

                    * Mode 2: No reconnection attempts remain.

                        * return None to proceed with raising a SubinitialNetworkException
                        * return 1+ to specify a number of additional reconnection attempts, thereby prolonging the
                          reconnection attempt procedure. 
        """

    def connect(self, host=NO_CHANGE):
        host = host or self.host
        if self.is_connected:
            if host == self.host:
                return False
            else:
                self.close()
        self.host = host

        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.settimeout(self.timeout)
        try:

            self.socket.connect((self.ipaddr, self.port))
            return True
        except Exception as ex:
            msg = "Failed to connect to {} at host: {}, timeout={}s, {}".format(
                    self.devicename, self.host, self.timeout, ex)
            raise SubinitialException(msg)
        # if self.protocol != DEFAULT:
        #     self.socket.sendall(struct.pack("B"))
        #     return self.socket.recv(BUFFER_SIZE)

    def stream(self, *channels):
        if not self.is_connected:
            self.connect()

        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(None)
        sock.connect((self.host, self.port))
        try:
            sock.sendall(_protocol_change(2) + _json2bytes(channels))
            x = sock.recv(4096)
            resp = _bytes2json(x)
            if resp == "ok":
                while True:
                    data = sock.recv(4096)
                    msg = _bytes2json(data)
                    yield msg
        finally:
            sock.close()

    def close(self):
        if self.is_connected:
            self.is_connected = False
            if self.socket:
                self.socket.shutdown(socket.SHUT_RDWR)
                self.socket.close()
                self.socket = None
            return True
        return False

    def _reconnect(self, tries: int or None = 1) -> int or None:
        while True:
            if self.on_networkfailure:
                retval = self.on_networkfailure(self, self.FAILUREMODE_RECONNECT)
                if type(retval) == int:
                    tries = retval
                if tries == 0:
                    return 0
            try:
                self.close()
                self.connect()
                return tries
            except Exception as e:
                self.device.logger.warning("Reconnecting to {} failed: {}".format(self.devicename, e))
                if tries is None:
                    continue
                tries -= 1
                if tries <= 0:
                    return 0

    def txrx(self, msg):
        # First Attempt
        try:
            self.socket.sendall(msg)
            return self.socket.recv(BUFFER_SIZE)
        except Exception as e:
            self.device.logger.warning("{} transmission failed: {}".format(self.devicename, e))
            ex = e

        # Retries
        retries = self.retries
        while True:
            # Try to reconnect if retries remain
            if retries is None or retries > 0:
                self.device.logger.warning("Reconnecting to {}...".format(self.devicename))
                retries = self._reconnect(retries)  # reconnecting may have consumed some retries

            # Notify upper layer of network failure
            if retries <= 0 and self.on_networkfailure:
                retval = self.on_networkfailure(self, self.FAILUREMODE_NO_RETRIES_REMAIN)
                if type(retval) == int:
                    retries = retval
                retries = self._reconnect(retries)
                if retries:  # if the handler has modified the retry count then try to reconnect
                    continue

            # We have run out of retries, time to quit
            if retries <= 0:
                break

            # Now that we are reconnected retry the transmission
            try:
                self.socket.sendall(msg)
                return self.socket.recv(BUFFER_SIZE)
            except Exception as e:
                self.device.logger.warning("{} transmission failed: {}".format(self.devicename, e))
                ex = e
            retries -= 1  # the above transmission failed, consume one retry

        raise ex  # SubinitialException("network connection failed")

    def request(self, *args):
        req = json.dumps(args, separators=(',', ':')) + "\n"
        msg = req.encode('utf-8')
        rsp = self.txrx(msg)
        if rsp and len(rsp):
            if rsp == b'_':  # "_" character denotes empty responses
                return None
            resp = rsp.decode()
            if resp.startswith("error: ") and self.raise_response_exceptions:
                raise SubinitialResponseError(resp[7:])
            elif resp.startswith("["):
                resp = json.loads(resp)
                if len(resp) == 1:
                    return resp[0]
            elif resp.startswith("{"):
                resp = json.loads(resp)
            return resp
        raise SubinitialResponseError("Remote device closed the network connection")


class LanDevice:
    NO_CHANGE = None
    DEFAULT = None
    ASSEMBLY = "SA00XXX"
    FIRMWARE_MIN = (2, 0, 0)

    def __init__(self, host: str, autoconnect=True):
        """Construct a Subinitial Lan Device instance

        :param host: mDNS hostname, NETBIOS hostname, or ip address of the device
        :param autoconnect: if True a network connection is made to the device on object initialization
        """
        self.verify_on_connect = True
        """By default, LanDevices will verify proper library/firmware version compatibility upon connecting"""
        self.logger = WarnLogger()
        """By default, LanDevices will log connection failure warnings and retries to stderr"""
        self.connection = Connection(self, host)
        self._request: Callable = self.connection.request
        self.system = System(self)

        if autoconnect:
            self.connect(host)

    def __enter__(self):
        self.connect()
        return self

    def __exit__(self, type_, value, traceback):
        self.close()

    @property
    def is_connected(self):
        return self.connection.is_connected

    def connect(self, host=DEFAULT, verify_connection=DEFAULT):
        if self.connection.connect(host):
            if verify_connection is False:
                return True
            if verify_connection or self.verify_on_connect:
                if not self.verify_connection():
                    self.close()
                    raise SubinitialException("Device verification failed")
            return True
        return False

    def close(self):
        return self.connection.close()

    def verify_connection(self) -> bool:
        """Verifies network connection to the device
            Also verifies if firmware version and library version are compatible.
            This prompts the user when incompatibilities are detected.

        :returns: True if connection verified, False if connection could not be verified
        """
        try:
            version = self.system.get_version()

            if self.ASSEMBLY != LanDevice.ASSEMBLY and version["assembly"] != self.ASSEMBLY:
                print("\nWARNING: Python is expecting {} with part-number: {}\nDevice is reporting part-number: {}"
                      .format(self.connection.devicename, self.ASSEMBLY, version["assembly"]))
                time.sleep(0.01)
                resp = input("Continue anyway? (y or n):")
                if 'y' in resp.lower():
                    return True
                else:
                    return False

            if tuple(version["firmware"]) < self.FIRMWARE_MIN:
                vstr = "v{}.{}.{}".format(*version["firmware"])
                fminstr = "v{}.{}.{}".format(*self.FIRMWARE_MIN)
                print("\nWARNING: {}\nFirmware should be updated to version >= {}".format(
                    self.connection.devicename, fminstr))
                time.sleep(0.01)
                resp = input("Update this device's firmware from {} to {}? (y or n):".format(vstr, fminstr))
                if 'y' in resp.lower():
                    if not self.system._upgrade(fminstr):
                        print("ERROR: firmware upgrade failed")
                        return False
                    version = self._request("version?")

            if LIB_VERSION < tuple(version["lib_min"]):
                vstr = "v{}.{}.{}".format(*LIB_VERSION)
                fminstr = "v{}.{}.{}".format(*version["lib_min"])
                print("\nWARNING: {}\nyour Python library should be updated from {} to version >= {}.".format(
                    self.connection.devicename, vstr, fminstr))
                time.sleep(0.01)
                resp = input("Continue anyway? (y or n):")
                if 'y' in resp.lower():
                    pass
                else:
                    return False
        except Exception as ex:
            print("An error occured while verifying connection to {}:\n\t{}".format(self.__class__.__name__, ex))
            return False
        return True


class Scope:
    def __init__(self, dev: LanDevice):
        self._dev = dev
        self._request = dev.connection.request


class Scpi(Scope):
    def get_idn(self) -> str:
        """Get the device's IDN

        :return: device IDN
        """
        return self._request("*idn?")


class Network(Scope):
    class Config(NamedTuple):
        dhcp: bool
        ipaddr: str
        subnet: str
        gateway: str
        dns: str
        hostname: str

    def config(self, dhcp: bool = NO_CHANGE, ipaddr: str = NO_CHANGE, subnet: str = NO_CHANGE,
                      gateway: str = NO_CHANGE, dns: str = NO_CHANGE, hostname: str = NO_CHANGE):
        """Set the device's network configuration to use automatic DHCP or static IP/Subnet/Gateway/DNS server
            the configuration will be applied immediately, and Python will attempt to connect to the new configuration
            parameters. If dhcp==True then Python will look for the hostname using mDNS and netbios
        :param dhcp: if True, use DHCP, if False, use static ip/subnet/gateway/dns
        :param ipaddr: static ipv4 ip address used if no DHCP, e.g. "192.168.1.51"
        :param subnet: static ipv4 subnet mask used if no DHCP, e.g. "255.255.255.0"
        :param gateway: static gateway ipv4 address used if no DHCP, e.g. "192.168.1.1"
        :param dns: static dns ip address used if no DHCP, e.g. "8.8.8.8"
        :param hostname: hostname, e.g. "mydevice"
            Note: the mDNS hostname == hostname + ".local"
                unless there are mDNS hostname conflicts on your LAN.
                In which case the device will be hostname + "-#.local"
                where # is a positive integer such that no conflicts exist on your LAN.
            Note: the NETBIOS name == hostname.
        """
        ret = self._request(":system:network:config", dhcp, ipaddr, subnet, gateway, dns, hostname)
        etc = ret.get("etc", 0)
        return etc

    def get_config(self) -> Config:
        """Gets the saved network settings applied at next device boot.

        :return: dictionary with the stored network settings for the device. These may not be the actual
            state of the device's network interface, check get_netstatus for that.
        """
        dhcp, ipaddr, subnet, gateway, dns, hostname = self._request(":system:network:config?")
        return self.Config(to_bool(dhcp), ipaddr, subnet, gateway, dns, hostname)

    def get_status(self) -> Config:
        """Gets the current network interface status.
        """
        dhcp, ipaddr, subnet, gateway, dns, mdns_hostname = self._request(":system:network:status?")
        return self.Config(to_bool(dhcp), ipaddr, subnet, gateway, dns, mdns_hostname)


class Usb(Scope):
    USB_OFF = "off"
    USB_MULTI = "g_multi"
    USB_ETHERNET = "g_ether"
    USB_MASS_STORAGE = "g_mass_storage"
    USB_SERIAL = "g_serial"

    class Config(NamedTuple):
        mode: str
        dhcp_server: bool
        ipaddr: str

    def mode(self, mode: str = USB_MULTI):
        """Set the device's usb mode configuration
        :param mode: usb device operating mode
            USB_OFF: the device will not enumerate as any usb device on your PC
            USB_MULTI: the device will enumerate as a usb-serial device, usb-ethernet device, and usb-mass-storage device
            USB_ETHERNET: the device will enumerate as just a usb-ethernet device
            USB_MASS_STORAGE: the device will enumerate as just a usb-mass-storage device
            USB_SERIAL: the device will enumeraet as just a usb-serial device
        """
        self._request(":system:usb:mode {}".format(mode))

    def get_config(self) -> Config:
        """Get the device's usb mode state

        :return: dictionary containing the current usb mode status
        """
        usb_mode, usb_dhcp_server, usb_ip = self._request("system:usb:config?")
        return self.Config(usb_mode, usb_dhcp_server, usb_ip)


class System(Scope):
    def __init__(self, dev):
        Scope.__init__(self, dev)
        self.usb = Usb(dev)
        self.network = Network(dev)
        self.scpi = Scpi(dev)

    def get_version(self):
        return self._request(":system:version?")

    def get_config(self):
        return self._request(":system:config?")

    # def set_hostname(self, hostname=DEFAULT):
    #     """Set the device's hostname, netbios name, and zeroconf/mdns name
    #         Note: the mDNS hostname == hostname + ".local"
    #             unless there are mDNS hostname conflicts on your LAN.
    #             In which case the device will be hostname + "-#.local"
    #             where # is a positive integer such that no conflicts exist on your LAN.
    #         Note: the NETBIOS name == hostname.
    #     :param hostname: desired hostname
    #     :type: str
    #     """
    #     self._request(":system:hostname {}".format(hostname))
    #
    # def get_hostname(self):
    #     """Get the device's hostname.
    #         Note: the mDNS hostname == hostname + ".local"
    #             unless there are mDNS hostname conflicts on your LAN.
    #             In which case the device will be hostname + "-#.local"
    #             where # is a positive integer such that no conflicts exist on your LAN.
    #         Note: the NETBIOS name == hostname.
    #
    #     :return: device hostname
    #     :rtype: str
    #     """
    #     return self._request(":system:hostname?")

    def identify(self, state: bool, off_delay: float or None = None):
        """Command the device identification led to flash, or stop flashing

        :param state: if True then identify LED will start flashing, if False then identify LED will stop flashing
        :param off_delay: if None, the LED will continue flashing until commanded False, if off_delay is a positive
            number then the LED will continue flashing for off_delay seconds before it stops flashing.
        """
        if off_delay is not None:
            self._request(":system:identify", state, off_delay)
        else:
            self._request(":system:identify", state)

    def get_identify(self) -> bool:
        """Get the device's identify state

        :return: True if device's identify LED is flashing
        """
        return to_bool(self._request(":system:identify?"))

    def reboot(self):
        """Perform a complete system reboot, the device will go offline momentarily and Python will
            attempt reconnection as it becomes available"""
        retval = self._request(":system:reboot")
        if retval.get("error"):
            self._dev.close()
            raise SubinitialResponseError(retval["error"])
        etc = retval["etc"]
        if etc:
            self._dev.close()
            self._dev.logger.warning("waiting {}s for reset".format(etc))
            time.sleep(etc)
            self._dev.connect()
        return

    def _shutdown(self):
        """Shutdown device, after which a power-cycle will be required for the device to resume operations"""
        self._request(":system:shutdown")
        self._dev.close()

    def _upgrade(self, version=""):
        """Perform a software upgrade"""
        self._dev.connection.socket.settimeout(15)
        retval = self._request(":system:upgrade", version)
        if retval["error"]:
            self._dev.close()
            raise SubinitialResponseError(retval["error"])
        etc = retval["etc"]
        if etc:
            etc += 4
            self._dev.logger.warning("closing")
            self._dev.close()
            self._dev.logger.warning("waiting {}s".format(etc))
            time.sleep(etc)
            self._dev.logger.warning("reconnecting")
            self._dev.connect()
            self._dev.logger.warning("connection succeeded")

        return retval


if sys.version_info >= (3, 0):
    long = int


class Bitmask(long):
    """
    32-bit Bitmask that acts like an integer with added helper methods.

    .. Note:: Accessing an element of Bitmask at index i returns the bit state at position i.

        e.g. Bitmask(0b10)[1] == True

    .. Note:: Iterating over Bitmask yields bit state at positions 0 through 31.
    """
    def __getitem__(self, index):
        """Access element at index.

        :param int index: bit index
        :return: bit value at specified bit index
        :rtype: bool
        """
        return ((1 << index) & self) > 0

    def __iter__(self):
        for _i in range(0, 32):
            yield _i, self[_i]

    def list_of_setbits(self):
        """Get list of bit indices that are set to 1.

        :return: list of set bit indices, e.g. 0b1010 -> [1, 3]
        :rtype: list(int)
        """
        return list(idx for idx, is_set in self if is_set)

    def extract(self, position, mask):
        """Extract a bitfield out of this 32bit integer bitmask.

        :param position: rightmost bit position of the bitfield
        :param mask: right aligned mask of bitfield length, e.g. 3-bit bitfield mask == 0b111
        :return: Bitmask
        :rtype: int | Bitmask
        """
        return Bitmask((self >> position) & mask)

    def insert(self, position, mask, value):
        """Insert a bitfield into this 32bit integer bitmask.

        :param position: rightmost bit position of the bitfield
        :param mask: right aligned mask of bitfield length, e.g. 3-bit bitfield mask == 0b111
        :param value: bitfield value to be inserted
        :return: Bitmask
        :rtype: int | Bitmask
        """
        t = self & ~(mask << position)
        return Bitmask(t | ((value & mask) << position))

    def setmask(self, mask):
        """Return new Bitmask with mask set.

        :param mask: mask of bits to be set, e.g. mask=0b01 sets bit 0.
        :return: Bitmask
        :rtype: int | Bitmask
        """
        return Bitmask(self | mask)

    def clearmask(self, mask):
        """Return new Bitmask with mask cleared.

        :param mask: mask of bits to be cleared, e.g. mask=0b01 clears bit 0.
        :return: Bitmask
        :rtype: int | Bitmask
        """
        return Bitmask(self & ~mask)

    def to_uint8_array(self):
        return [
            self & 0xFF,
            (self >> 8) & 0xFF,
            (self >> 16) & 0xFF,
            (self >> 24) & 0xFF
        ]

    def to_uint16_array(self):
        return [self & 0xFFFF, (self >> 16) & 0xFFFF]


def mask_to_bits(mask):
    bits = []
    for i in range(0, 32):
        if (1 << i) & mask > 0:
            bits.append(i)
    return bits


def digest_indices(indices, mask, clearmask=0xFFFFFFFF, return_mask_not_list=True, idx2mask=lambda idx:1<<idx):
    if len(indices) > 0:
        if mask > 0:
            raise SubinitialInputError("Cannot use both indices and bitmask as arguments. Pick one.")
        try:
            for index in indices:
                mask |= idx2mask(index)
        except Exception:
            raise SubinitialInputError("Invalid argument. Indices must be integers.")
    mask &= clearmask
    if return_mask_not_list:
        return mask
    else:
        return mask_to_bits(mask)


def do_for(timeout, func, func_args_list=None, startdelay=0, repeatdelay=0.005):
    """
    :param timeout: float, time in seconds after which
    :param func: callable, function that must return bool, or an iterable with bool as its first element.
    A return value of True indicates the function has completed successfully.
    A return value of False indicates the function must be called again after a delay "cycledelay" so long as "timeout"
    seconds have not yet elapsed since the first call to "func".
    :param func_args_list: list or tuple, arguments that will be passed to func
    :param startdelay: float, optional time in seconds to delay before starting the first call to func
    :param repeatdelay: float, time in seconds to delay before repeating the next call to func
    :return: the last value returned from func before timing out or upon func completing successfully
    """
    func_retval = None
    if startdelay > 0:
        time.sleep(startdelay)
    start_time = time.time()
    while True:
        if func_args_list is None:
            func_retval = func()
        else:
            func_retval = func(*func_args_list)
        if func_retval is not None:
            if hasattr(func_retval, '__getitem__'):
                if func_retval[0] is True:
                    break
            elif func_retval:
                break
        if time.time() - start_time >= timeout:
            break
        if repeatdelay > 0:
            time.sleep(repeatdelay)
    return func_retval
