# Copyright (c) 2020 Kyle Howen
# All Rights Reserved.

import os
from setuptools import setup, find_packages


def read(*rnames):
    return open(os.path.join(os.path.dirname(__file__), *rnames)).read()

setup(
    name='subinitial.tracks',
    version='2.0.0',
    description='Subinitial product interface drivers & firmware management',
    packages=['subinitial.tracks', 'subinitial'],
    include_package_data=True,
    # namespace_packages=['subinitial'],
    license='UNLICENSED',
    url='https://bitbucket.org/subinitial/subinitial-tracks.git',
    author='Kyle Howen',
    author_email='kyle.howen@subinitial.com',
    long_description=read("README.md"),
    keywords="subinitial",
    classifiers = [
        # 'Development Status :: 4 - Beta',
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'License :: Other/Proprietary License',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
    ]
)
