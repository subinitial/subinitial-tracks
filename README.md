# Subinitial Tracks Library

Subinitial product information:
https://subinitial.com

Git repository:
https://bitbucket.org/subinitial/subinitial-tracks.git

## Introduction

This repository provides interface code and firmware for Subinitial products.
Please email support@subinitial.com for comments or questions

Python API documentation in the docstrings in this repository.

You'll find examples on using this API to control a device in the "examples/" directory within this repository.

## Requirements

- Python 3.6+
- **python3** and **pip3** on your system PATH.
- *Recommended:* mDNS support for LAN hostnames, e.g. "tracks0343.local" vs "192.168.1.48". GNU/Linux distributions & Mac usually have mDNS configured. For Windows, make sure you have [Bonjour Print Services for Windows](https://developer.apple.com/bonjour/) installed.

Use the commands below in a command prompt to verify your installed versions:

```sh
python --version
python3 --version
pip3 --version
```

## Installation

Install all requirements, then open a command prompt and run the **pip3** command below

```sh
pip3 install --user git+https://bitbucket.org/subinitial/subinitial-tracks.git
```

NOTES:

- pip's `--user` flag allows you to install this package without admin privileges,
  If you'd like to install the package system-wide then omit the `--user` flag e.g. `pip3 install git+https://bitbucket.org/`...
- You can use virtualenv to make a localized Python environment for a particular project then
  pip install all required packages as needed with the virtualenv activated. Omit the `--user`
  flag when installing this package inside a virtualenv.
- You can distribute a specific version of the library with your Python code easily 
  by using the command below. The command below creates the **subinitial-tracks** package from the 
  git tag **v1.1.0** in the directory "**.**" which is the current working directory (CWD)

```sh
pip3 install git+https://bitbucket.org/subinitial/subinitial-tracks.git@v1.1.0 --target="."
```

## Verify Installation

For a product configured to IP address 192.168.1.48:

```python
#! /usr/bin/env python3
from subinitial.tracks import Tracks
tracks = Tracks(host="192.168.1.48")
```
