# -*- coding: UTF-8 -*-
# SiRIP Kit Solid State Relays example
# © 2012-2020 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

from subinitial.tracks import Tracks

""" Connect to the device """
# You can access the device by IP address (e.g. 192.168.1.49) or mDNS name
# For mDNS on windows, make sure you have "Bonjour Print Services for Windows" installed
#   Find it at: https://developer.apple.com/bonjour/
serial_number = "0617"
mdns_hostname = f"tracks{serial_number}.local"
tracks = Tracks(mdns_hostname)


""" BASIC USAGE """
tracks.cmd(0)  # command relay 0 on, all others off
tracks.set(1, 4)  # set relay 1 and 4 on, 0 will remain on, all others should still be off
tracks.set(mask=0xFF000000)  # engage all relays 24 through 31 using a bitmask


""" Get Relay Status """
channel_status = tracks.get_cmd()
print(f"Get commanded state for all tracks relays: {hex(channel_status)} // {channel_status.list_of_setbits()}")


""" Clear individual channels """
tracks.clear(mask=0xF)  # disengage relays 0 through 3 using a bitmask


""" Get Relay Status """
channel_status = tracks.get_cmd()
print(f"Get commanded state after CH0 and 1 are cleared: {hex(channel_status)} // {channel_status.list_of_setbits()}")


""" Turn off all channels """
tracks.clear(mask=tracks.ALL_CHANNELS_MASK)


""" Get Relay Status """
channel_status = tracks.get_cmd()
print(f"Get commanded state after all channels are cleared: {hex(channel_status)} // {channel_status.list_of_setbits()}")
